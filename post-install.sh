#! /bin/bash

swapoff /dev/sda2 > /dev/null 2>&1
parted /dev/sda rm 2 > /dev/null 2>&1
sed -i '/swap/d' /etc/fstab 2 > /dev/null 2>&1

cat > /etc/resolv.conf <<EOF
nameserver 208.67.222.222 
nameserver 208.67.220.220
nameserver 213.186.33.99
search educatux.org
EOF

systemctl disable named
systemctl stop named
pacman --noconfirm -Rns bind

pacman --noconfirm -Syyy htop tree mc vim git ansible python-docker zsh zsh-syntax-highlighting inxi parted lsof strace p7zip
wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc

chsh -s /bin/zsh root

git config --global user.name "Stéphane (Educatux) GAUTIER"
git config --global user.email "framagit@educatux.org"

install -d /srv/ansible

sed -i "s/^#fr_FR.UTF-8/fr_FR.UTF-8/" /etc/locale.gen > /dev/null 2>&1
/usr/bin/locale-gen > /dev/null 2>&1

echo "LANG=fr_FR.UTF-8" > /etc/locale.conf
echo "KEYMAP=fr" > /etc/vconsole.conf

cat > /etc/ansible/hosts <<EOF
[local]
localhost ansible_connection=local

[local:vars]
ansible_python_interpreter=/usr/bin/python3
EOF